# Kaplan International DevOps Test #

We believe in infrastructure as code, we create our Puppet manifests and test them locally on vagrant boxes, then deploy our changes gradually to test, stage, production environments. 
This ensures we can rebuild all of our infrastructure from scratch, at any time.
Hopefully this test will give you an idea of our workflow and tools we use. 
Don’t feel like you have to follow any of the suggestions given, if you know a better way, just tell us why you did it that way.

The evaluation has three components; it is not necessary to complete all of them however.

### Task 1 Puppet ###
Requirements:

Create a puppet module called 'devops' that does the following:

* installs apache
* Creates a vhost that points to /var/www/devopstest/
* Grabs the file from https://s3-eu-west-1.amazonaws.com/kaplandevopstest/version.json and upload it to /var/www/devopstest/
* starts apache and ensures it is on at startup

Deliverables:

* A pull request (PR) from a branch named 'puppet'
* The virtual machine should boot and provision the services with the command 'vagrant up'.
* you should be able to access the Vagrant box's vhost from localhost on the host machine

Bonus:

* your code should pass the puppet-lint style guide

### Task 2 Python ###
Requirements:

Create a simple Python script that, when run locally, will check the version.json file served by the Vagrant box created in Task 1 and return the version

Deliverables:

* A pull request (PR) from a branch named 'python'
* A python script in a folder called /scripts

Bonus:

* your code should pass the PEP8 style guide
* your script should compare the version.json served by the Vagrant box, with a version passed in as an argument

### Task 3 CloudFormation (Optional) ###
Requirements:

Build a cloudformation stack consisting of:

* An EC2 Instance
* A Load Balancer (ELB)

Deliverables:

* A pull request (PR) from a branch named 'cloudformation'
* A cloudformation JSON script that will launch an EC2 instance behind an ELB in a folder called /cloudformation