# /manifests/default.pp


# fixes default virtual_packges to false as per the older puppet versions
if versioncmp($::puppetversion,'3.6.1') >= 0 {
  $allow_virtual_packages = hiera('allow_virtual_packages',false)

  Package {
    allow_virtual => $allow_virtual_packages,
  }
}

node 'default'{
  include devops
}

